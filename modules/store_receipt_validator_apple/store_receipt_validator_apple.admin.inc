<?php
/**
 * @file
 * Admin file.
 */

/**
 * Configuration form.
 */
function store_receipt_validator_apple_config_form($form, &$form_state) {

  // Decide if use apple sandbox endpoint or not.
  $form['store_receipt_validator_apple_sandbox'] = array(
    '#title'         => t('Use sandbox'),
    '#description'   => t('Use sandbox API.'),
    '#type'          => 'checkbox',
    '#default_value' => variable_get('store_receipt_validator_apple_sandbox', TRUE),
  );

  // Shared secret.
  $form['store_receipt_validator_apple_shared_secret'] = array(
    '#title'         => t('Shared secret'),
    '#description'   => t('Shared secret.'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('store_receipt_validator_apple_shared_secret', NULL),
  );
  return system_settings_form($form);
}
